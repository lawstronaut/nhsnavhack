from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

# class User(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     username = db.Column(db.String(80), unique=True)
#     email = db.Column(db.String(120), unique=True)
#     nationality = db.Column(db.string(80), unique=True)
#     rating = db.Column(db.Integer, unique=True)

#         def __init__(self, username, email, nationality, rating):
#         self.username = username
#         self.email = email
#         self.nationality = nationality
#         self.rating = rating

#     def __repr__(self):
#         return '<User %r>' % self.username

# class QuestionBank(db.question):
# 	id = db.Column(db.Integer, primary_key=True)
# 	Question = db.Column(db.String(120), unique=True)
# 	Answer = db.Column(db.String(120), unique=True)

#     def __init__(self, question, answer):
#         self.Question = question
#         self.Answer = answer

#     def __repr__(self):
#         return '<Question %r>' % self.Question



@app.route("/")
def index():
	# db.create_all()
	return render_template('index.html')



@app.route("/signup", methods=['GET', 'POST'])
def signup():
	# if request.method == 'POST':
	# 	add_to_db(request.form['email', request.form['password'], request.form['languages']])
	return render_template('signupMentors.html')
    
@app.route("/mentors")
def mentors():
	return render_template('mentors.html')  
    
    
@app.route("/question")
def questions():
	return render_template('question.html')     
    
    
@app.route("/topic")
def topics():
	return render_template('topic.html')     
        
    


# def add_to_db(email, password, nationality):
# 	db.session.add(User(email, password, nationality))


if __name__ == "__main__":
	app.run(debug=True)

