$(document).ready(function () {
       var rows = $('#online-professionals > tbody > tr a');
       console.log('Rows: ', rows);
       var userId = "Sam";
       var ai1 = "Caleb Sanchez";
       var ai2 = "Musa Ahmad Urdu";
       var ai3 = "Korinn Pinakoulaki";
       var ai4 = "Zach Jones";
       var box = null;
       var box2 = null;
       var box3 = null;
       var box4 = null;

       $("#caleb_sanchez").click(function (event, ui) {
           if (box) {
               box.chatbox("option", "boxManager").toggleBox();
           }
           else {
               box = $("#chat_div0").chatbox({
                   id: "Sam",
                   user: {key: "value"},
                   title: ai1,
                   offset: 0,
                   messageSent: function (id, user, msg) {
                       $("#chat_div0").chatbox("option", "boxManager").addMsg(userId, msg);

                       if (msg == 'hello') {
                           $("#chat_div0").chatbox("option", "boxManager").addMsg(ai1, "Hello, how may I help?");
                       } else if (msg == 'what does GP mean?') {
                           $("#chat_div0").chatbox("option", "boxManager").addMsg(ai1, "GP is someone who prescribes paracetamols to patiants regardless of their symptoms.");
                       } else if (msg == 'bye') {
                           $("#chat_div0").chatbox("option", "boxManager").addMsg(ai1, "Bye for now. Have a good day.");
                       }
                   },
                   boxClosed: function(id) {
                       console.log('Chat box closed');
                   }
               });
           }
       });
	   

       $("#korinn_pinakoulaki").click(function (event, ui) {
           if (box2) {
               box2.chatbox("option", "boxManager").toggleBox();
           }
           else {
               box2 = $("#chat_div2").chatbox({
                   id: "Sam",
                   user: {key: "value"},
                   title: ai3,
                   offset: 310,
                   messageSent: function (id, user, msg) {
                       $("#chat_div1").chatbox("option", "boxManager").addMsg(userId, msg);

                       if (msg == 'hello') {
                           $("#chat_div2").chatbox("option", "boxManager").addMsg(ai3, "Hello, how may I help?");
                       } else if (msg == 'what does GP mean?') {
                           $("#chat_div2").chatbox("option", "boxManager").addMsg(ai3, "GP is someone who prescribes paracetamols to patiants regardless of their symptoms.");
                       } else if (msg == 'bye') {
                           $("#chat_div2").chatbox("option", "boxManager").addMsg(ai3, "Bye for now. Have a good day.");
                       }
                   },
                   boxClosed: function(id) {
                       console.log('Chat box closed');
                   }
               });
           }
       });
	   

       $("#musa_ahmad_urdu").click(function (event, ui) {
           if (box4) {
               box4.chatbox("option", "boxManager").toggleBox();
           }
           else {
               box4 = $("#chat_div1").chatbox({
                   id: "Sam",
                   user: {key: "value"},
                   title: ai2,
                   offset: 620,
                   messageSent: function (id, user, msg) {
                       $("#chat_div1").chatbox("option", "boxManager").addMsg(userId, msg);

                       if (msg == 'hello') {
                           $("#chat_div1").chatbox("option", "boxManager").addMsg(ai2, "Hello, how may I help?");
                       } else if (msg == 'what does GP mean?') {
                           $("#chat_div1").chatbox("option", "boxManager").addMsg(ai2, "GP is someone who prescribes paracetamols to patiants regardless of their symptoms.");
                       } else if (msg == 'bye') {
                           $("#chat_div1").chatbox("option", "boxManager").addMsg(ai2, "Bye for now. Have a good day.");
                       }
                   },
                   boxClosed: function(id) {
                       console.log('Chat box closed');
                   }
               });
           }
       });

       $("#zach_jones").click(function (event, ui) {
           if (box3) {
               box3.chatbox("option", "boxManager").toggleBox();
           }
           else {
               box3 = $("#chat_div3").chatbox({
                   id: "Sam",
                   user: {key: "value"},
                   title: ai4,
                   offset: 930,
                   messageSent: function (id, user, msg) {
                       $("#chat_div3").chatbox("option", "boxManager").addMsg(userId, msg);

                       if (msg == 'hello') {
                           $("#chat_div3").chatbox("option", "boxManager").addMsg(ai4, "Hello, how may I help?");
                       } else if (msg == 'what does GP mean?') {
                           $("#chat_div3").chatbox("option", "boxManager").addMsg(ai4, "GP is someone who prescribes paracetamols to patiants regardless of their symptoms.");
                       } else if (msg == 'bye') {
                           $("#chat_div3").chatbox("option", "boxManager").addMsg(ai4, "Bye for now. Have a good day.");
                       }
                   },
                   boxClosed: function(id) {
                       console.log('Chat box closed');
                   }
               });
           }
       });
   });